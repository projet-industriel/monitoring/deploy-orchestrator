# Déploiement de l'orchestrateur Kubernetes pour le monitoring à partir d'un pipeline sur Gitlab  

__École__ : IMT Atlantique  
__Formation__ : Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__ : 2022-2023  
__UE__ : Projet industriel  
__Dépôt Gitlab__ : <https://gitlab.imt-atlantique.fr/projet-industriel/monitoring/deploy-orchestrator>  
__Date__ : 27 Mars 2023  

## Description

Ce projet permet de déployer et maintenir automatiquement l'orchestrateur Kubernetes sur l'infrastructure du monitoring.  

* L'infrastruction du projet "deploy-infrastructure" du groupe "monitoring-logging" doit être préalablement mise en place.  

* L'inventaire de l'infrastructure est généré par le projet "deploy-infrastructure"  

* Les opérations sont effectuées à partir des playbooks Ansible.  

* Les tâches à exécuter sont organisées dans des rôles.  

* Les playbooks sont variabilisés pour permettre la personnalisation de l'environnement de production.  

* La connexion SSH aux instances est configurée à l'aide du fichier __ssh.conf.template__ et des variables d'environnement. Le fichier __ssh.conf__ va être généré et utilisé par la configuration d'Ansible.  

* Un Dockerfile permet de construire l'image des Jobs  

## Prérequis

* Le déploiement de l'infrastructure du projet "deploy-infrastruction"  
* Le package registry du projet "deploy-infrastruction":  
  * la disponibilité du fichier __hosts.ini__ du package  
* La clé privée SSH de connexion aux instances  
* Un token d'accès à l'api du "projet-industriel" des droits "owner" pour assurer l'utilisation du package registry  

## Configurer le déploiement Ansible

* Modifier le fichier __group_vars/all/main.yml__ pour personnaliser les tâches à effectuer par Ansible.  
* Modifier le fichier __group_vars/all/proxy.yml__ pour personnaliser les proxy des machines hôtes.  
* Modifier le fichier __roles/cluster_generator/cluster.yml.j2__ pour personnaliser le fichier du cluster utilisé par Rancher.  

> Remarque: Ne pas toucher au fichier __group_vars/all/env.yml.template__. Il est configuré dans la chaine de déploiement.  

## Configurer l'environnement du pipeline sur Gitlab

### Créer ou mettre à jour les variables Gitlab du groupe "monitoring-logging"

* Se rendre dans l'onglet "Settings > CI/CD" du groupe.  
* Dans la section "Variables", modifier/ajouter les variables suivantes:  
  * __PRIVATE_KEY__: contenu de la clé privée SSH (Utiliser une variable de type "File")  
  * __BASTION_USER__: utilisateur SSH bastion  
  * __NODE_USER__: utilisateur SSH des instances  
  * __BASTION_PUBLIC_IP__: adresse IP publique de l'instance bastion  
  * __TF_PROJECT_ID__: identifiant du projet deploy-infrastructure  
  * __TF_PKG_NAME__: nom du package registry des fichiers d'inventaire générés par deploy-infrastructure.  
  * __TF_PKG_VERSION__: version du package registry

### Créer ou mettre à jour les variables Gitlab du groupe parent "projet-industriel"

* Se rendre dans l'onglet "Settings > CI/CD" du groupe.
  * __API_TOKEN__: token d'accès à l'API Gitlab du groupe avec les privilèges "owner"  

### Vérifier l'inventaire

Il faut s'assurer que le fichier d'inventaire __hosts.ini__ de l'infrastructure est disponible dans le package registry __infra-observ__ du projet "deploy-infrastructure".  

## Déployer l'orchestrateur Kubernetes dans l'infrastructure de monitoring

* Se rendre dans l'onglet "CI/CD > pipeline" du projet.  
* Cliquer sur le bouton __"Run pipeline"__.  
* Sur la fénètre qui s'affiche, cliquer à nouveau sur le bouton __"Run pipeline"__ pour valider l'opération sans renseigner de variables.  

Dans le pipeline,  

* déclencher le job __configuration__ pour configurer les prérequis du déploiement  
* déclencher le job __docker__ pour installer docker sur les instances  
* déclencher le job __proxy__ pour configurer les proxy des instances  
* déclencher le job __rke_up__ pour déployer Kubernetes à partir de Rancher  

## Maintenance de l'orchestrateur Kubernetes dans l'infrastructure de monitoring

Dans le pipeline précédent,  

* déclencher le job __rke_update__ pour mettre à jour le cluster Kubernetes  
* déclencher le job __rke_remove__ pour détruire le cluster Kubernetes au besoin  
